from django.shortcuts import render
from .models import Post
from django.core.paginator import Paginator


def index(request):
    posts = Post.objects.all()

    paginator = Paginator(posts, 5)
    page = request.GET.get('page')
    posts = paginator.get_page(page)

    return render(request,
                  'blog/index.html',
                  {'items': posts})
